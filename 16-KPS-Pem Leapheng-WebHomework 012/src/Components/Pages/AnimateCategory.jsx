import React from 'react'
import { Link, BrowserRouter,useRouteMatch, Switch ,Route} from 'react-router-dom'
import { Container } from 'react-bootstrap'
import Topic from './Topic';
export default function AnimateCategory() {
    let { path, url } = useRouteMatch();

  
    return (
        <Container>
            <div>
                <BrowserRouter>
                    <h4>Animation Category</h4>
                    <ul>
                        <li><Link to={`${url}/action`}>Action</Link></li>
                        <li><Link to={`${url}/romance`}>Romance</Link></li>
                        <li><Link to={`${url}/comedy`}>Comedy</Link></li>
                    </ul>
                    <Switch>
                        <Route exact path={path}><h3>Please Select A topic</h3></Route>
                        <Route path={`${path}/:topicName`}><Topic></Topic></Route>
                    </Switch>
                </BrowserRouter> 
            </div>
        </Container>
    )
}
