import React, { Component } from "react";
import ProductCard from "./ProductCard";

class Home extends Component {
  render() {
    let products = this.props.products.map((product) => (
      <ProductCard key={product.id} product={product} />
    ));
    return (
      <div className="container-fluid">
        <div className="row">{products}</div>
      </div>
    );
  }
}

export default Home;
