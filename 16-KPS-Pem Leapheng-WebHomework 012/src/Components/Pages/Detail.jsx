import React from "react";

export default function Detail({ match }) {
  console.log(match.params.id);
  return (
    <div style={{ textAlign: "center" }}>
      <h4>This Content from Product {match.params.id}</h4>
    </div>
  );
}
