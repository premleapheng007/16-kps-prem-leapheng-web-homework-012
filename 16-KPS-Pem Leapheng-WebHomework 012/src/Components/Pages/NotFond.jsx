import React from "react";

export default function NotFond() {
  return (
    <div style={{ textAlign: "center" }}>
      <h1>This page is not exist in our system </h1>
    </div>
  );
}
