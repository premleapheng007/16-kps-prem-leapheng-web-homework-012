import React from "react";
import { Container } from "react-bootstrap";
import { Link, useRouteMatch, useLocation } from "react-router-dom";
import AccountName from "./AccountName";
export default function Account(props) {
  let { url } = useRouteMatch();
  let value = useQuery();
  return (
    <Container>
      <div>
        <h3>Accounts</h3>
        <ul>
          <li>
            <Link to={`${url}?name=netflix`}>Netflix</Link>
          </li>
          <li>
            <Link to={`${url}?name=zilow-group`}>Zillow Group</Link>
          </li>
          <li>
            <Link to={`${url}?name=yahoo`}>Yahoo</Link>
          </li>
          <li>
            <Link to={`${url}?name=modus-create`}>Modus Create</Link>
          </li>
        </ul>
        <AccountName name={value.get("name")} />
      </div>
    </Container>
  );
}
function useQuery() {
  return new URLSearchParams(useLocation().search);
}
