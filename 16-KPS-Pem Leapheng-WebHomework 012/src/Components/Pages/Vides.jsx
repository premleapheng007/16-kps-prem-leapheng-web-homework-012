import React from "react";
import { Container } from "react-bootstrap";
import {
  Link,
  Switch,
  BrowserRouter,
  Route,
  useRouteMatch,
} from "react-router-dom";
import AnimateCategory from "./AnimateCategory";
import MovieCategory from "./MovieCategory";

export default function Vides() {
  let { path, url } = useRouteMatch();
  return (
    <Container>
      <div className="mt-3">
        <BrowserRouter>
          <ul>
            <li>
              <Link to={`${url}/animation`}>Animation</Link>
            </li>
            <li>
              <Link to={`${url}/movie`}>Movies</Link>
            </li>
          </ul>
          <Switch>
            <Route exact path={path}>
              <h3>Please Selcet A topic</h3>
            </Route>
            <Route path={`${path}/animation`} component={AnimateCategory} />
            <Route path={`${path}/movie`} component={MovieCategory} />
          </Switch>
        </BrowserRouter>
      </div>
    </Container>
  );
}
