import ItemNavbar from "./Components/ItemNavbar";
import { Switch, BrowserRouter, Route } from "react-router-dom";
import Main from "./Components/Pages/Main";
import Home from "./Components/Pages/Home";
import Detail from "./Components/Pages/Detail";
import NotFond from "./Components/Pages/NotFond";
import Vides from "./Components/Pages/Vides";
import Account from "./Components/Pages/Account";
import AnimateCategory from "./Components/Pages/AnimateCategory";
import MovieCategory from "./Components/Pages/MovieCategory";
import Welcome from "./Components/Pages/Welcome";
import Auth from "./Components/Pages/Auth";
import React, { Component } from "react";
import { ProtectedRoute } from "./Components/Pages/Route/Proected";

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      product: [
        {
          id: 1,
          src:
            "https://ncxhonda.com.kh/wp-content/uploads/2019/10/Elegance-Black.png",
          name: "HONDA DREAM125",
          des: "Price : 2210 Whith 2 Color",
        },
        {
          id: 2,
          src:
            "https://ncxhonda.com.kh/wp-content/uploads/2019/01/WAVE100-2019.png",
          name: "WAVE100",
          des: "Price : 1970 With 3 Color",
        },
        {
          id: 3,
          src:
            "https://ncxhonda.com.kh/wp-content/uploads/2020/05/LUXURIOUS-WHITE.png",
          name: "SCOOPY I",
          des: "Price : 2150 With 2 Color",
        },
        {
          id: 4,
          src:
            "https://ncxhonda.com.kh/wp-content/uploads/2020/01/Ultimate-White.png",
          name: "PCX150",
          des: "Price : 3500 With 1 Color",
        },
        {
          id: 5,
          src:
            "https://ncxhonda.com.kh/wp-content/uploads/2019/11/Advanture-Red-400x400.png",
          name: "ADV150",
          des: "Price : 2050 With 3 Color",
        },
        {
          id: 6,
          src: "https://ncxhonda.com.kh/wp-content/uploads/2019/03/MSXRED-.png",
          name: "MSX150SF",
          des: "Price : 2420 With 3 Color",
        },
        {
          id: 7,
          src:
            "https://ncxhonda.com.kh/wp-content/uploads/2019/03/CBR150R-Black.png",
          name: "CB150R",
          des: "Price : 2550 With 2 Color",
        },

        {
          id: 8,
          src:
            "https://ncxhonda.com.kh/wp-content/uploads/2019/11/Ultimate-White-400x400.png",
          name: "CLICK150I",
          des: "Price : 2600 With 2 Color",
        },
      ],
    };
  }
  render() {
    return (
      <div>
        <BrowserRouter>
          <ItemNavbar />
          <Switch>
            <Route exact path="/" component={Main} />
            <Route
              path="/home"
              render={() => <Home products={this.state.product} />}
            ></Route>
            <Route path="/videos" component={Vides} />
            <Route path="/account" component={Account} />
            <Route path="/auth" render={() => <Auth />} />
            <Route path="/posts/:id" component={Detail} />
            <ProtectedRoute exact path="/welcome" component={Welcome} />
            <Route path="/videos/animation" component={AnimateCategory} />
            <Route path="/videos/movie" component={MovieCategory} />
            <Route path="*" component={NotFond} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}
